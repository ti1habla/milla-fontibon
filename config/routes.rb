Rails.application.routes.draw do
  resources :contacts
  get '/404', to: 'errors#not_found'
  get '/422', to: 'errors#unacceptable'
  get '/500', to: 'errors#internal_error'

  get "site/index", as: 'home'
  get "site/enconstruccion"
  post "/save_form", to:"site#save_form"
  get '/proyecto/armonia', to: 'site#armonia'
  get '/proyecto/arandanos', to: 'site#arandanos'
  get '/proyecto/riovivo', to: 'site#riovivo'
  get '/proyecto/olivar', to: 'site#olivar'
  get '/proyecto/armonia/gracias', to: 'site#gracias'
  get '/proyecto/arandanos/gracias', to: 'site#gracias'
  get '/proyecto/riovivo/gracias', to: 'site#gracias'
  get '/proyecto/olivar/gracias', to: 'site#gracias'

  get '/tyc', to: 'site#tyc'

  root "site#index"

end
