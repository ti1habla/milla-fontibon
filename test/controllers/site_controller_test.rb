require 'test_helper'

class SiteControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get site_index_url
    assert_response :success
  end

  test "should get enconstruccion" do
    get site_enconstruccion_url
    assert_response :success
  end

end
