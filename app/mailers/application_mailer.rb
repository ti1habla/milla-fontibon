class ApplicationMailer < ActionMailer::Base
  default from: 'Habla Creativo <bot@hablacreativo.com>'
  layout 'mailer'
end
