class FormMailer < ApplicationMailer

    def nuevo_contacto(datos)
        @datos = datos
        mail to: [@datos[:destinatarios], "contactos@contex.com.co"], subject: "[Millafontibon.co] Nuevo contacto desde el sitio web", from: 'Habla Creativo <bot@hablacreativo.com>'
    end

    def email_a_contacto(datos)
        @datos = datos
        mail to: @datos[:email], subject: "[Millafontibon.co] Hemos recibido tu información", from: @datos[:proyecto]+' <'+@datos[:remitente]+'>'
    end

end

