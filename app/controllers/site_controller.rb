class SiteController < ApplicationController

  def index
    @proyectos = [
      {
        url: '/proyecto/armonia',
        imagen: '/sitio/proyectos/p1.png'
      },
      {
        url: '/proyecto/riovivo',
        imagen: '/sitio/proyectos/p2.png'
      },
      {
        url: '/proyecto/arandanos',
        imagen: '/sitio/proyectos/p3.png'
      },
      {
        url: '/proyecto/olivar',
        imagen: '/sitio/proyectos/p4.png'
      }
    ]
    # abort @proyectos.inspect
  end

  def enconstruccion
  end

  def save_form
    # params[:formlanding]["name"]

    if params[:formlanding]["project"] == "Olivar"
      @remitente = "bot@hablacreativo.com"
      @destinatario = "olivar@contex.com.co"
      @proyecto = "Olivar"
      @redirect_path = "/proyecto/olivar/gracias"
      @brochure = 'brochure/olivar.pdf'
    elsif params[:formlanding]["project"] == "Arándanos"
      @remitente = "bot@hablacreativo.com"
      @destinatario = "arandanos@contex.com.co"
      @proyecto = "Arándanos"
      @redirect_path = "/proyecto/arandanos/gracias"
      @brochure = 'brochure/arandanos.pdf'
    elsif params[:formlanding]["project"] == "Armonía"
      @remitente = "bot@hablacreativo.com"
      @destinatario = "lafortunaventas@verticeing.com"
      @proyecto = "Armonía"
      @redirect_path = "/proyecto/armonia/gracias"
      @brochure = 'brochure/armonia.pdf'
    elsif params[:formlanding]["project"] == "Rio Vivo"
      @remitente = "bot@hablacreativo.com"
      @destinatario = "riovivo@arconsa.com.co"
      @proyecto = "Rio Vivo"
      @redirect_path = "/proyecto/riovivo/gracias"
      @brochure = 'brochure/riovivo.pdf'
    end

    @data = {
      nombre: params[:formlanding]["name"] + " " + params[:formlanding]["lastname"],
      email: params[:formlanding]["email"],
      cel: params[:formlanding]["cel"],
      proyecto: params[:formlanding]["project"],
      destinatarios: @destinatario,
      remitente: @remitente,
      brochure: @brochure
    }

    @new_contact = Contact.new
    @new_contact.nombre = @data[:nombre]
    @new_contact.email = @data[:email]
    @new_contact.cel = @data[:cel]
    @new_contact.proyecto = @data[:proyecto]
    @new_contact.save
    
    FormMailer.nuevo_contacto(@data).deliver_now
    FormMailer.email_a_contacto(@data).deliver_now
    redirect_to @redirect_path, notice: @proyecto
  end

  def armonia
    @proyecto = "Armonía"
  end

  def arandanos
    @proyecto = "Arándanos"
  end

  def olivar
    @proyecto = "Olivar"
  end

  def riovivo
    @proyecto = "Rio Vivo"
  end

end



