class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts do |t|
      t.string :nombre
      t.string :email
      t.string :cel
      t.string :proyecto

      t.timestamps
    end
  end
end
